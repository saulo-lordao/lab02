# -*- coding: UTF-8 -*-

import codecs
import functools
from collections import defaultdict
from math import floor

import nltk
from nltk.collocations import *

def compose(*functions):
    return functools.reduce(lambda f, g: lambda x: f(g(x)), functions, lambda x: x)

def lines(corpus):
    return corpus.split("\n")

def unlines(corpus):
    return " ".join(corpus)

def mapT(f):
    return lambda (a,b): (f(a), f(b))

def lines(corpus):
    return corpus.split("\n")

def unlines(corpus):
    return " ".join(corpus)

def tokenize(corpus):
    return nltk.tokenize.word_tokenize(corpus)

def remove_stopwords(corpus):
    stopwords = nltk.corpus.stopwords.words("portuguese")
    return filter(lambda w: w not in stopwords, corpus)

def mapT(f):
    return lambda (a,b): (f(a), f(b))

def load_corpus(name):
    with codecs.open("corpus_%s/corpus.txt" % name, "r", encoding="utf-8") as file:
        return file.read().lower()

def make_sets(tokenized_corpus, cutoff=0.8):
    n = len(tokenized_corpus)
    cutoff = int(floor(n * cutoff))
    return (tokenized_corpus[:cutoff], tokenized_corpus[cutoff:])

def make_prob_dist(freq_dist):
    N = float(len(freq_dist))
    prob = defaultdict(lambda: 0.01)
    for word in freq_dist:
        prob[word] = freq_dist[word] / N
    return prob

def perplexity(testset, prob_dist):
    perplexity = float(1)
    probs = []
    for word in testset:
        probs.append((word, prob_dist[word]))
        perplexity *= 1/prob_dist[word]
    perplexity = pow(perplexity, 1/float(len(prob_dist)))
    return (perplexity, probs)

prep_corpus = compose(remove_stopwords, tokenize, unlines)
load_sets = compose(mapT(prep_corpus), make_sets, lines, load_corpus)

tecnologia = load_sets("tecnologia")
